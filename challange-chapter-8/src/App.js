import './App.css';
import Layout from './components/Layout';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import { Navigate, Outlet, Route, Routes} from "react-router-dom";

function App() {
  const ProtectedRoute = ({ children}) => {
    const accessToken = localStorage.getItem('accessToken');

    if (!accessToken) {
      return <Navigate to={"/login"} />;
    } else {
      return children;
    }
  }
  

  return (
    <Routes>
      <Route path="/home" element={<div>Home tanpa layout</div>} />
      <Route path="/" element={<Layout />}>
        <Route path="" element={<div>root path kosong</div>}/>
        <Route 
          path="/dashboard" 
          element={
            <ProtectedRoute>
            <div>
              Ini dashboard
            </div>
            </ProtectedRoute>
          }
        />
        <Route path="/login" element={<Login/>}/>
        <Route path="/registration" element={<div>Ini adalah halaman register</div>}/>
        <Route path="/user">
          <Route path="" element={<div>Ini user</div>}/>
          <Route path="profile" element={<div>Ini adalah profile user</div>}/>
          <Route path="score" element={<div>Ini adalah score user</div>}/>
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
