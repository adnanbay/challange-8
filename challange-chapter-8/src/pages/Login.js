import React from 'react';
import { useState } from "react";
import { useNavigate } from 'react-router-dom';

export default function Login() {

    const [username, setUsername] = useState(null);
    const navigate = useNavigate();

    const handleLogin = () => {
        // Call API untuk cek validasi Login
        // kalau ada access token disimpan pada local storage

        const accessToken = username; //for now username jadi akses token
        localStorage.setItem('accessToken', accessToken);
        navigate("/dashboard");
    }

  return (
    <div style= {{marginTop: "50px"}}>
        <center>
            <div><h3>LOGIN PAGE</h3></div>
            <div><input 
                placeholder="Username"
                onChange={(e) => {
                    setUsername(e.target.value);
                }}
            />
            <input 
                placeholder="Password"
                onChange={(e) => {
                    setUsername(e.target.value);
                }}
            />
            <button onClick={handleLogin}>Submit</button>
            </div>
        </center>
    </div>

  )
}
